from typing import Tuple

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


class NrsrDataset(object):

    def __init__(self, url, min_length=250, min_speeches=20):
        self._url = url
        self._min_length = min_length
        self._min_speeches = min_speeches
        self._X = None
        self._y = None

    def get_subset_of_classes(self, classes_count=None) -> Tuple[np.ndarray, np.ndarray]:
        if self._not_loaded():
            self._load()

        if classes_count is not None:
            X = self._X[self._y < classes_count]
            y = self._y[self._y < classes_count]
        else:
            X, y = self._X, self._y
        return X, y

    def _not_loaded(self) -> bool:
        return self._X is None

    def _load(self):
        df = pd.read_json(self._url, orient='records', encoding='utf8')
        X = df['speech'].values
        y = df['speaker'].values
        X, y = self._filter_short_speeches(X, y)
        X, y = self._filter_low_amount_of_speeches(X, y)
        y = self._encode_labels(y)
        self._X = X
        self._y = y

    def _filter_short_speeches(self, X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        np_len = np.vectorize(len)
        have_min_len = np_len(X) >= self._min_length
        X = X[have_min_len]
        y = y[have_min_len]
        return X, y

    def _filter_low_amount_of_speeches(self, X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        _, idx, count = np.unique(y, return_inverse=True, return_counts=True)
        X = X[np.in1d(idx, np.where(count >= self._min_speeches)[0])]
        y = y[np.in1d(idx, np.where(count >= self._min_speeches)[0])]
        return X, y

    @staticmethod
    def _encode_labels(y: np.ndarray) -> np.ndarray:
        label_encoder = LabelEncoder()
        y = label_encoder.fit_transform(y)
        return y
