from typing import Tuple

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import scale
from sklearn.svm import LinearSVC, SVC

from theanosvc import linearsvc

IRIS_URL = 'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'
RANDOM_STATE = 0


def main():
    X, y = get_iris_dataset()
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=RANDOM_STATE, stratify=y, test_size=0.2)

    weights, accuracy = train_sklearn_linearsvm(X_train, X_test, y_train, y_test)
    print('Accuracy sklearn.LinearSVC: %.2f' % accuracy)
    plot_boundary('sklearn.LinearSVC', X, y, weights)

    weights, accuracy = train_sklearn_svm(X_train, X_test, y_train, y_test)
    print('Accuracy sklearn.SVC: %.2f' % accuracy)
    plot_boundary('sklearn.SVC', X, y, weights)

    weights, accuracy = train_theano_linearsvm(X_train, X_test, y_train, y_test)
    print('Accuracy theanosvc.LinearSVC: %.2f' % accuracy)
    plot_boundary('theanosvc.LinearSVC', X, y, weights)


def get_iris_dataset() -> Tuple[np.ndarray, np.ndarray]:
    df = pd.read_csv(IRIS_URL, header=None)
    X = df.iloc[:, [0, 2]].values
    y = df.iloc[:, 4].values
    y = np.where(y == 'Iris-setosa', -1, 1)
    X = scale(X)
    return X, y


def train_sklearn_linearsvm(
        X_train: np.ndarray, X_test: np.ndarray,
        y_train: np.ndarray, y_test: np.ndarray) -> Tuple[np.ndarray, float]:
    cls = LinearSVC(C=10)
    cls.fit(X_train, y_train)
    y_pred = cls.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    weights = np.concatenate([cls.intercept_, cls.coef_[0]])
    return weights, accuracy


def train_sklearn_svm(
        X_train: np.ndarray, X_test: np.ndarray,
        y_train: np.ndarray, y_test: np.ndarray) -> Tuple[np.ndarray, float]:
    cls = SVC(C=10, kernel='linear')
    cls.fit(X_train, y_train)
    y_pred = cls.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    weights = np.concatenate([cls.intercept_, cls.coef_[0]])
    return weights, accuracy


def train_theano_linearsvm(
        X_train: np.ndarray, X_test: np.ndarray,
        y_train: np.ndarray, y_test: np.ndarray) -> Tuple[np.ndarray, float]:
    cls = linearsvc.LinearSVC(C=10, max_iter=100)
    cls.fit(X_train, y_train)
    y_pred = cls.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    weights = cls.weights
    return weights, accuracy


def plot_boundary(title: str, X: np.ndarray, y: np.ndarray, weights: np.ndarray):
    plt.scatter(X[y == 1, 0], X[y == 1, 1], c='red')
    plt.scatter(X[y == -1, 0], X[y == -1, 1], c='blue')
    boundary_x = np.array([X[:, 0].min() - 1.0, X[:, 0].max() + 1])
    boundary_y = get_y_linfunc(boundary_x, weights)
    plt.plot(boundary_x, boundary_y)
    plt.title(title)
    plt.show()
    plt.close()


def get_y_linfunc(x: np.ndarray, w: np.ndarray) -> np.ndarray:
    a = - w[1] / w[2]
    b = - w[0] / w[2]
    y = x * a + b
    return y


if __name__ == '__main__':
    main()
