import os
from time import time
from typing import Tuple

import numpy as np
import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from theanosvc.linearsvc import LinearSVC

BASE_PATH = os.path.dirname(__file__)
NRSR_DATSET_PATH = os.path.join(BASE_PATH, 'nrsr.json')


def get_nrsr_dataset(path, min_length=250, min_speeches=20, max_class_id=100000) -> Tuple[np.ndarray, np.ndarray]:
    df = pd.read_json(path, orient='records', encoding='utf8')

    X = df['speech'].values
    y = df['speaker'].values

    np_len = np.vectorize(len)
    have_min_len = np_len(X) >= min_length
    X = X[have_min_len]
    y = y[have_min_len]

    _, idx, count = np.unique(y, return_inverse=True, return_counts=True)
    X = X[np.in1d(idx, np.where(count >= min_speeches)[0])]
    y = y[np.in1d(idx, np.where(count >= min_speeches)[0])]

    label_encoder = LabelEncoder()
    y = label_encoder.fit_transform(y)

    X = X[y <= max_class_id]
    y = y[y <= max_class_id]

    return X, y


def main():
    print('Loading data')
    X, y = get_nrsr_dataset(NRSR_DATSET_PATH, max_class_id=20, min_speeches=20)

    print('Vectorizing')
    vectorizer = TfidfVectorizer(norm=None, use_idf=True, ngram_range=(1, 1))
    X = np.asarray(vectorizer.fit_transform(X).todense())

    print('SVD')
    svd = TruncatedSVD(n_components=200)
    X = svd.fit_transform(X)

    print('Preparing classifier')
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, test_size=0.2, random_state=0)

    start = None
    repeats = 10

    for _ in range(repeats):
        print('Training my SVC')
        svc = LinearSVC(C=1)
        svc.fit(X_train, y_train)

        print('Evaluating')
        y_pred = svc.predict(X_test)
        score = accuracy_score(y_test, y_pred)
        print(score)

        if start is None:
            start = time()

    end = time()
    print('Time per one execution:', (end - start) / repeats - 1)

    '''
    print('Training Sklearn Linear SVC')
    svc = svm.LinearSVC(C=1)
    svc.fit(X_train, y_train)
    y_pred = svc.predict(X_test)
    score = accuracy_score(y_test, y_pred)
    print(score)
    '''


if __name__ == '__main__':
    main()
