FROM continuumio/miniconda3
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    g++ \
    graphviz

RUN conda install \
    numpy \
    pandas \
    scikit-learn \
    theano \
    pydot

RUN wget -O /root/nrsr.json https://svananrsr.blob.core.windows.net/datasets/nrsr.json
ADD .theanorc /root/.theanorc
ADD . /app

WORKDIR /app
VOLUME /results

ENTRYPOINT ["python3", "-u", "main.py"]
