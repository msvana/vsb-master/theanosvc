"""
Configuration for NRSR benchmark
"""

RESULTS_PATH = '/results'
NRSR_DATASET_URL = '/root/nrsr.json'
MIN_SPEECH_LENGTH = 350
MIN_SPEECHES = 50
N_FEATURES = 300
