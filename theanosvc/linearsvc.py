import numpy as np
import theano as th
import theano.tensor as T
from theano import compile

T_FLOAT = th.config.floatX
ARRAY_2D = (1, 1)
ARRAY_1D = (1,)


class LinearSVC(object):
    """
    Linear support vector classifier implemented in Theano
    Optimizes L(w,D) = 1/2*||w||^2 + C*sum(max(0, 1-yi*xi*w)) cost function using gradient descent
    Inspired by https://github.com/eakbas/tf-svm
    """

    def __init__(self, eta=0.1, C=1.0, max_iter=800, tol=0.001):
        """
        :param float eta: initial learning rate. Decreases by 1% each epoch
        :param float C: L2 regularization strenght - lower number means stronger regularization
        :param int max_iter: maximum number of learning epochs
        :param float tol: Stoping criteria tolerance. Training stops if relative change in cost is less then 'tol'
        """
        self._max_iter = max_iter
        self._tol = tol
        self._costs = []
        self._eta_base = eta
        self._init_basic_graph_parts(C)

    def fit(self, X, y):
        """
        Fit the model according to provided training data

        :param np.ndarray X: Matrix of training data features. Shape = [n_samples, n_features]
        :param np.ndarray y: Vector of target classes for training data Shape = [n_samples]
        :return: Trained model (self)
        :rtype: LinearSVC
        """
        uq_classes = np.unique(y).astype(T_FLOAT)
        self._w.set_value(np.random.rand(len(uq_classes), X.shape[1] + 1).astype(T_FLOAT))
        self._prev_costs.set_value(np.zeros(len(uq_classes), T_FLOAT))
        fit_graph = self._build_fit_graph(X, y, uq_classes)\

        self._costs = []

        for i in range(self._max_iter):
            costs = fit_graph()
            multipliers = costs[1]
            self._costs.append(costs[0])

            if len(self._costs) < 2:
                continue

            all_classifiers_reached_tol = np.all(multipliers == 0)

            if all_classifiers_reached_tol:
                break

        print("Iterations: ", self._iters.get_value())
        return self

    def predict(self, X):
        """
        Predict class for given samples. Does not use Theano

        :param np.ndarray X: Matrix of samples. Shape = [n_samples, n_features]
        :return: Predicted class labels for each sample
        :rtype: np.ndarray
        """
        ones = np.ones((X.shape[0], 1))
        aug_X = np.hstack((ones, X))
        w = self._w.get_value()
        net_input = np.dot(w, aug_X.T)
        y_pred = np.argmax(net_input, axis=0)
        return y_pred

    @property
    def weights(self):
        """
        Shape = [n_classes, n_features + 1]. Bias is on index 0

        :rtype: np.ndarray
        """
        return self._w.get_value()

    @property
    def costs(self):
        """
        Shape = [n_epochs, n_classes]

        :rtype: list of float
        """
        return self._costs

    @property
    def iters(self):
        """
        Number of epochs until the last training stopped

        :rtype: int
        """
        return self._iters

    def _init_basic_graph_parts(self, C):
        """
        Initializes basic data structures required by the algorithm

        :param float C: Regularization strength, see __init__()
        """
        self._X = th.shared(np.zeros(ARRAY_2D, T_FLOAT), name='X')
        self._y = th.shared(np.zeros(ARRAY_1D, T_FLOAT), name='y')
        self._w = th.shared(np.zeros(ARRAY_2D, T_FLOAT), name='w')

        self._aug_X = self._augment_x()
        self._uq_classes = th.shared(np.zeros((1,), dtype=T_FLOAT), name='uq_classes')
        self._uq_classes_cnt = th.shared(np.zeros((1,), dtype='int32'), name='uq_classses_cnt')
        self._prev_costs = th.shared(np.zeros(ARRAY_1D, dtype=T_FLOAT), name='prev_costs')

        self._iters = th.shared(np.array(0, dtype=T_FLOAT), name='iters')
        self._C = th.shared(np.array(C, dtype=T_FLOAT), name='C')

    def _build_fit_graph(self, X, y, uq_classes):
        """
        Builds Theano compute graph for model training

        :param np.ndarray X: Training samples
        :param np.ndarray y: Target classes
        :param np.ndarray uq_classes: Vector of unique class labels in the training dataset

        :return: Callable Theano function
        :rtype: compile.Function
        """
        costs = self._cost()
        total_cost = T.sum(costs)
        cost_grad = T.grad(total_cost, self._w)
        costs_change = abs(costs - self._prev_costs) / self._prev_costs
        multipliers = T.where(T.gt(costs_change, self._tol), 1.0, 0.0)
        eta_modifer = 350 / (200 + self._iters)
        eta = self._eta_base * multipliers * eta_modifer

        updates = [
            (self._prev_costs, costs),
            (self._w, self._w - eta.reshape((-1, 1)) * cost_grad),
            (self._iters, self._iters + 1)
        ]

        fit_graph = th.function(
            inputs=[],
            outputs=[costs, multipliers],
            givens={
                self._X: X.astype(T_FLOAT),
                self._y: y.astype(T_FLOAT),
                self._uq_classes: uq_classes,
                self._uq_classes_cnt: np.array([len(uq_classes)], dtype='int32'),
            },
            updates=updates)

        return fit_graph

    def _y_ovr(self):
        """
        Creates a matrix containing modified class labels for each OvR classifier.
        First index (row) is equal to the class label, second index (column) is the sample number.
        Label is set to 1 if a sample belongs to the class defined by row number otherwise it is set to -1.

        :rtype: T.TensorVariable
        """
        y_tiled = T.tile(self._y, [self._uq_classes_cnt[0], 1])
        uq_classes_matrix = T.reshape(self._uq_classes, [1, self._uq_classes_cnt[0]])
        y_ovr = y_tiled - uq_classes_matrix.T
        y_ovr = T.where(T.eq(y_ovr, 0.0), 1.0, -1.0)
        return y_ovr

    def _cost(self):
        """
        Calculates cost function by adding regularization and hinge cost.
        Also takes regularization strength (C) into consideration.

        :rtype: T.TensorVariable
        """
        regularization_cost = self._regularization_cost()
        hinge_cost = self._hinge_cost()
        cost = regularization_cost + self._C * hinge_cost
        return cost

    def _regularization_cost(self) -> T.TensorVariable:
        """ Calculates L2 regularization cost """
        regularization_cost = 0.5 * self._w.norm(L=2, axis=1)
        return regularization_cost

    def _hinge_cost(self) -> T.TensorVariable:
        """
        Calculates hinge cost - penalization for missclassification
        proportional to the distance from hyperplane
        """
        functional_margins = 1 - T.dot(self._w, self._aug_X.T) * self._y_ovr()
        cost_per_sample = 0.5 * (functional_margins + abs(functional_margins))
        hinge_cost = T.sum(cost_per_sample, axis=1)
        return hinge_cost

    def _augment_x(self) -> T.TensorVariable:
        """ Adds column of ones to the features matrix. This allows w0 to be used as bias """
        ones = T.ones_like(self._y)
        ones_matrix = T.shape_padright(ones)
        augmented_x = T.concatenate([ones_matrix, self._X], axis=1)
        return augmented_x
