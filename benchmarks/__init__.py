from . import base
from . import theano
from theanosvc import linearsvc
from sklearn import svm

benchmarks = {
    'linearsvc': base.Benchmark('LinearSVC', svm.LinearSVC()),
    'svc': base.Benchmark('SVC', svm.SVC(kernel='linear')),
    'theanosvc': theano.Benchmark('TheanoSVC', linearsvc.LinearSVC),
    'theanosvc2': theano.Benchmark('TheanoSVC', linearsvc.LinearSVC),
    'theanosvc4': theano.Benchmark('TheanoSVC', linearsvc.LinearSVC)
}
