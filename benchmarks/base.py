from time import time
from typing import List, Tuple

import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

import config
from utils.nrsr import NrsrDataset


class Benchmark(object):
    _class_range = range(2, 162, 4)

    def __init__(self, name, classifier, repeats=1):
        self._name = name
        self._repeats = repeats
        self._classifier = classifier

    def run(self, dataset: NrsrDataset) -> List[Tuple]:
        print('===================================================')
        print('%s benchmark' % self._name)

        results = []

        for class_count in self._class_range:
            print('---------------------------------------------------')
            X, y = dataset.get_subset_of_classes(class_count)
            print('Number of classes: ', class_count)
            print('Number of samples: ', len(y))

            X_train, X_test, y_train, y_test = self._preprocess_dataset(X, y)
            print('Running benchmark')
            start = time()

            for _ in range(self._repeats):
                self._benchmark(X_train, X_test, y_train, y_test)

            end = time()
            self._clean()
            duration = end - start
            print('Duration: %.5f' % duration)
            results.append((len(y), duration))

        return results

    @staticmethod
    def _preprocess_dataset(X: np.ndarray, y: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        print('Vectorizing')
        vectorizer = TfidfVectorizer(norm=None, use_idf=True, ngram_range=(1, 1))
        X = vectorizer.fit_transform(X)

        print('Selecting best features')
        selector = SelectKBest(k=config.N_FEATURES)
        X = selector.fit_transform(X, y).toarray()

        print('Creating training and testing sets')
        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, test_size=0.1, random_state=0)
        return X_train, X_test, y_train, y_test

    def _benchmark(self, X_train: np.ndarray, X_test: np.ndarray, y_train: np.ndarray, y_test: np.ndarray):
        self._classifier.fit(X_train, y_train)
        y_pred = self._classifier.predict(X_test)
        score = accuracy_score(y_test, y_pred)
        print('Score: ', score)

    def _clean(self):
        pass
