import os
from . import base
import numpy as np
from sklearn.metrics import accuracy_score


class Benchmark(base.Benchmark):

    def _benchmark(self, X_train: np.ndarray, X_test: np.ndarray, y_train: np.ndarray, y_test: np.ndarray):
        cls = self._classifier()
        cls.fit(X_train, y_train)
        y_pred = cls.predict(X_test)
        score = accuracy_score(y_test, y_pred)
        print('Score: ', score)

    def _clean(self):
        print('Clearing cache')
        os.system('theano-cache clear')
