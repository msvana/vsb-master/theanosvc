import os
import sys

import mkl
import numpy as np
import pandas as pd

import config
from benchmarks import benchmarks
from utils.nrsr import NrsrDataset


def main():
    mkl.set_num_threads(2)

    dataset = NrsrDataset(config.NRSR_DATASET_URL, config.MIN_SPEECH_LENGTH, config.MIN_SPEECHES)
    benchmark_name = sys.argv[1]
    benchmark = benchmarks[benchmark_name]
    results = benchmark.run(dataset)
    save_results(benchmark_name, results)


def save_results(benchmark_name, results):
    results_file = os.path.join(config.RESULTS_PATH, '%s.csv' % benchmark_name)
    results = np.array(results)

    try:
        df = pd.read_csv(results_file)
        df[str(df.shape[1])] = pd.Series(results[:, 1], index=df.index)
    except FileNotFoundError:
        df = pd.DataFrame(results)
        df.columns = ['samples', '1']

    df.to_csv(results_file, index=False)


if __name__ == '__main__':
    main()
