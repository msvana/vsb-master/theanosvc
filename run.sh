#!/usr/bin/env bash

docker kill research_theanosvc
docker rm research_theanosvc
docker build -t research_theanosvc .

for run in `seq 5`
do
  docker run -it --rm --name research_theanosvc -v /home/milos/data/theanosvc:/results --cpus 2 research_theanosvc theanosvc2
done



